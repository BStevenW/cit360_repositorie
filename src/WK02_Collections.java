import java.util.*;

public class WK02_Collections {

    public static void main(String[] args){

        //I Copied the first two from your example video, I am not very good at programming so this is hard for me.

        System.out.println("-- List --");
        List list = new ArrayList();
        list.add("Hello World!");
        list.add("What is it like to be human?");
        list.add("Does it hurt to breath?");
        list.add("Do you know what its like to die?");
        list.add("How does it feel to poop?");
        list.add("Us aliens dont know anything about you?");
        list.add("Can we experiment on you?");
        list.add("Can we experiment on you?");

        for (Object str : list) {
            System.out.println((String)str);
        }

        System.out.println("-- Queue --");
        Queue queue  = new PriorityQueue();
        queue.add("Hello World!");
        queue.add("What is it like to be human?");
        queue.add("Does it hurt to breath?");
        queue.add("Do you know what its like to die?");
        queue.add("How does it feel to poop?");
        queue.add("Us aliens dont know anything about you?");
        queue.add("Can we experiment on you?");
        queue.add("Can we experiment on you?");

        Iterator iterator = queue.iterator();
        while (iterator.hasNext()) {
            System.out.println(queue.poll());
        }

        //Found this code at https://www.javatpoint.com/java-treeset
        System.out.println("-- Tree Set --");
        TreeSet set = new TreeSet();
        set.add(24);
        set.add(274);
        set.add(12);
        set.add(999);
        set.add(420);
        set.add(485);
        set.add(792);

        System.out.println("Highest Value: "+set.pollFirst());
        System.out.println("Lowest Value: "+set.pollLast());
        //Not sure how to print out the list of numbers


        //https://www.geeksforgeeks.org/set-in-java/
        System.out.println("-- Set -- ");
        Set hash_Set = new HashSet();
        hash_Set.add("Hello World!");
        hash_Set.add("What is it like to be human?");
        hash_Set.add("Does it hurt to breath?");
        hash_Set.add("Do you know what its like to die?");
        hash_Set.add("How does it feel to poop?");
        hash_Set.add("Us aliens dont know anything about you?");
        hash_Set.add("Can we experiment on you?");
        hash_Set.add("Can we experiment on you?");

        System.out.println(hash_Set);
    }
}
